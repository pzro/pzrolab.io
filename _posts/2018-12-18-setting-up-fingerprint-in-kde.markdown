---
layout: post
title:  "Setting Up Fingerprint in KDE"

categories: kde
---
I followed the [fingerprint-gui ArchWiki page][fingerprint-gui-archwiki] and the [author's manual][fingerprint-gui-manual], but needed to do some fiddling around to get it working in KDE. The end result is not very seamless, and is kind of quirky. Sudo works fine. In sddm and the lockscreen, you press enter at the password box, then the scanner takes your fingerprint. I couldn't get it to work for the KDE su GUI prompt.

For this to work, your fingerprint sensor must have drivers in Linux, or maybe the fingerprint-gui package. Otherwise you may have to install it from elsewhere. 

Installation
------------
Install fingerprint-gui from the AUR (I was on Manjaro. There is a PPA for it too.) Delete 'polkit-kde-authentication-agent-1.desktop' in the directory '/etc/xdg/autostart/'

Configure fingerprint-gui
-------------------------
Open up fingerprint-gui as your user (don't sudo). Your fingerprint device must be listed, select it and click next. Follow the steps to set up your fingerprint.
 
If you get the error 'Couldn't access fingerprint device/Permission problem', that means your user does not have access to the fingerprint device. To give fingerprint device access to the user, add the user to the group 'plugdev'. If it still doesn't work, set up a udev rule that gives the user access to the fingerprint device. You can take the help of [this guide][udev-rules-archwiki].

In the 'Settings' tab, testing the services would not work, because we haven't set up the PAM configurations yet.

Configure PAM
-------------
Navigate to /etc/pam.d/ and edit the file 'system-auth'.
Add a line 'auth	sufficient  pam_fingerprint-gui.so -d  try_first_identified' after the line '#%PAM-1.0'.
Modify the line 'auth    required  pam_unix.so     try_first_pass nullok' to 'auth    sufficient  pam_unix.so     try_first_pass nullok'

The result should be:
{% highlight shell %}
#%PAM-1.0

auth	sufficient  pam_fingerprint-gui.so -d  try_first_identified
auth    sufficient  pam_unix.so     try_first_pass nullok
...
{% endhighlight %}

Done
----
Test it out in the fingerprint-gui 'Settings' tab. Reboot and try the other services.



[fingerprint-gui-archwiki]: https://wiki.archlinux.org/index.php/Fingerprint_GUI
[fingerprint-gui-manual]: http://www.ullrich-online.cc/fingerprint/doc/Step-by-step-manual.html
[udev-rules-archwiki]: https://wiki.archlinux.org/index.php/Udev#About_udev_rules